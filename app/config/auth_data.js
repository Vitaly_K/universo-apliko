const appSettings = require("tns-core-modules/application-settings");

export default {
  get: { headers: {  } },
  post: {
    headers: {
      //"X-Auth-Token": appSettings.getString("token"),
      "X-CSRFToken": "",
    }
  }
};
