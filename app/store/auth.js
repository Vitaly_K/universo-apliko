import axios from "../config/axios";
import headers from "../config/auth_data"
import Vue from "vue"

const appSettings = require("tns-core-modules/application-settings");

export default {
  namespaced: true,
  state() {
    return {
      token: "",
      csrfToken: "",
      user_data: {
        unuaNomo: {
          enhavo: ""
        },
        familinomo: {
          enhavo: ""
        },
        retpowto: ""
      },
      is_logged: null,
    };
  },
  getters: {
    user_data(state) {
      return state.user_data;
    },

    permissions(state) {
      if (state.user_data.hasOwnProperty('allPerms')) {
        return state.user_data.allPerms;
      }
      return {}
    },

    isLogged(state) {
      return state.is_logged;
    }
  },
  mutations: {
    set_user(state, data) {
      Vue.set(state, 'user_data', data ? data : {
        unuaNomo: {
          enhavo: ""
        },
        familinomo: {
          enhavo: ""
        }
      });
      Vue.set(state, 'is_logged', !!data);
    },
  },
  actions: {
    async fetch_user(store, data) {
      const query = `
query{
  mi {
    uuid
    objId
    allPerms
    retpowto
    unuaNomo{
      enhavo
    }
    duaNomo{
      enhavo
    }
    familinomo{
      enhavo
    }
    organizo {
      nomo {
        enhavo
      }
      propra
    }
    premioj(orderBy: ["stadio", "krea_dato"]) {
    	edges {
      	node {
        	nomo {
          	enhavo
          }
          priskribo {
            enhavo
          }
          dato
          kodo
          stadio
          bildo
          ricevita
          statuso
          nivelo
      	}
      }
    }
    auhtoritatoj
    avataro
    bonusojPunktoj
    vendoj
  }
}
    `;

      const conf = {};

      if (appSettings.hasKey("token")) {
        conf["headers"] = {
          "X-Auth-Token": appSettings.getString("token")
        };
      }

      return axios.post("/registrado/", {query: query}, conf)
        .then(response => {
          let resp = response.data.data.mi;
          // Регулярное выражение для поиска CSRF в переданных с сервера COOKIES
          const reg_exp = new RegExp(
            `^${axios.defaults.xsrfCookieName}=.+`,
            "gi"
          );
          // Находим CSRF
          // Обновляем CSRF в глобальных заголовках запросов для AXIOS - а вот это обязательно!
          headers.post.headers["X-CSRFToken"] = response.headers["set-cookie"]
            .filter(v => reg_exp.test(v))
            .map(v => v.split(";")[0].split("=")[1])[0];
          headers.post.headers["Referer"] = axios.defaults.baseURL;

          if (!!resp) {
            // Сохраняем пользователя
            resp.allPerms = JSON.parse(resp.allPerms);
            store.commit('set_user', resp);

            return Promise.resolve(true);
          }

          store.commit('set_user', resp);
          return Promise.resolve(false);
        })
        .catch(e => {
          return Promise.reject(e);
        });
    },

    login(store, data) {
      const ajaxData = {
        query: `
            mutation($login: String!, $password: String!){
              ensaluti(login: $login, password: $password) {
                status
                homo {
                  objId
                  unuaNomo {
                    lingvo
                    enhavo
                    chefaVarianto
                  }
                  familinomo {
                    lingvo
                    enhavo
                    chefaVarianto
                  }
                }
                token
                csrfToken
                message
              }
            }`,
        variables: {login: data.email, password: data.password}
      };

      return axios.post("/registrado/", ajaxData)
        .then(response => {
          const resp = response.data.data.ensaluti;

          if (resp.status) {
            appSettings.setString("token", resp.token);

            return Promise.resolve(resp.status);
          }

          return Promise.reject(new Error(resp.message))
        })
        .catch(e => {
          return Promise.reject(e)
        });
    },

    async logout(store) {
      appSettings.clear();
      const query = {
        query: `
mutation {
  elsaluti {
    status
    message
  }
}
      `
      };

      await axios.post('/registrado/', query, headers.post);

      store.commit('set_user', null);
    },
  }
};
