# Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!

Workers of the world, unite!

Пролетарии всех стран, соединяйтесь!

# Основная информация

Это репозиторий Мобильного приложения Реального виртуального мира Универсо (Universo) на NativeScript-Vue для Android и iOS.

Смотрите список активных Обсуждений (Issues) здесь в репозитории https://gitlab.com/tehnokom/universo-apliko/-/issues

Более подробная информация в главном репозитории Универсо вот тут по ссылке https://gitlab.com/tehnokom/universo

Так же смотрите репозиторий с задачами по наполнению Универсо и сбору предложений https://gitlab.com/tehnokom/universo-esplorado

Наш дискорд-сервер по Универсо https://discord.gg/bsvdPy5

# Использование

``` bash
# Install dependencies
npm install

# Preview on device
tns preview

# Build, watch for changes and run the application
tns run

# Build, watch for changes and debug the application
tns debug <platform>

# Build for production
tns build <platform> --env.production

```
